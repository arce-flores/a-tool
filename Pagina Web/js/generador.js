var res
const obtener = async file => {
    const response = await fetch(file)
    const text = await response.text()
    return text.toString()
}
async function crearZip(){
    cargarDatosGrafo()
    if(grafo.nodos.length>0 && grafo.inicio!=null){
        var zip = new JSZip();
        zip.file('adlcp_rootv1p2.xsd',await obtener('scorm/adlcp_rootv1p2.xsd'));
        zip.file('adlcp_v1p3.xsd',await obtener('scorm/adlcp_v1p3.xsd'));
        zip.file('adlnav_v1p3.xsd',await obtener('scorm/adlnav_v1p3.xsd'));
        zip.file('adlseq_v1p3.xsd',await obtener('scorm/adlseq_v1p3.xsd'));
        zip.file('datatypes.dtd',await obtener('scorm/datatypes.dtd'));
        zip.file('ims_xml.xsd',await obtener('scorm/ims_xml.xsd'));
        zip.file('imscp_rootv1p1p2.xsd',await obtener('scorm/imscp_rootv1p1p2.xsd'));
        zip.file('imscp_v1p1.xsd',await obtener('scorm/imscp_v1p1.xsd'));
        zip.file('imsmanifest.xml',await obtener('scorm/imsmanifest.xml'));
        zip.file('imsmd_rootv1p2p1.xsd',await obtener('scorm/imsmd_rootv1p2p1.xsd'));
        zip.file('imsss_v1p0.xsd',await obtener('scorm/imsss_v1p0.xsd'));
        zip.file('imsss_v1p0auxresource.xsd',await obtener('scorm/imsss_v1p0auxresource.xsd'));
        zip.file('imsss_v1p0control.xsd',await obtener('scorm/imsss_v1p0control.xsd'));
        zip.file('imsss_v1p0delivery.xsd',await obtener('scorm/imsss_v1p0delivery.xsd'));
        zip.file('imsss_v1p0limit.xsd',await obtener('scorm/imsss_v1p0limit.xsd'));
        zip.file('imsss_v1p0objective.xsd',await obtener('scorm/imsss_v1p0objective.xsd'));
        zip.file('imsss_v1p0random.xsd',await obtener('scorm/imsss_v1p0random.xsd'));
        zip.file('imsss_v1p0rollup.xsd',await obtener('scorm/imsss_v1p0rollup.xsd'));
        zip.file('imsss_v1p0seqrule.xsd',await obtener('scorm/imsss_v1p0seqrule.xsd'));
        zip.file('imsss_v1p0util.xsd',await obtener('scorm/imsss_v1p0util.xsd'));
        zip.file('lom.xsd',await obtener('scorm/lom.xsd'));
        zip.file('xml.xsd',await obtener('scorm/xml.xsd'));
        zip.file('XMLSchema.dtd',await obtener('scorm/XMLSchema.dtd'));
        var pag = zip.folder('Pagina Web')
        var css = pag.folder('css')
        var js = pag.folder('js')
        pag.file('herramientaScorm.html',await obtener('scorm/Pagina Web/herramientaScorm.html'))
        css.file('herramienta.css',await obtener('scorm/Pagina Web/css/herramienta.css'))
        js.file('herramientaScorm.js',await obtener('scorm/Pagina Web/js/herramientaScorm.js'))
        js.file('control.js',await obtener('scorm/Pagina Web/js/control.js'))
        js.file('funcionesScorm.js',await obtener('scorm/Pagina Web/js/funcionesScorm.js'))
        
        var g = JSON.stringify(grafo)
        zip.file('grafo.txt',g);
        
        zip.generateAsync({type:"blob"})
            .then(function(content) {
            saveAs(content, "scorm.zip");
        });
    }
    else{
        alert("FALTA COMPLETAR DATOS")
    }
}
function cargarDatosGrafo(){
    var e = document.getElementById("recorrido");
    grafo.tipo = e.value
}