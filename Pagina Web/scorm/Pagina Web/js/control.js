async function empezar(){
    document.getElementById("bBFS").style.visibility = "hidden";
    document.getElementById("bDFS").style.visibility = "hidden";
    await cargarGrafoCargado();
    colorearInicio()
    startTimeStamp = new Date();
    ScormProcessInitialize();
    marcarIncompleto()
    llenarLabel()
}
function terminar(pressedExit){
    if (processedUnload == true){return;}
    processedUnload = true;
    var endTimeStamp = new Date();
    var totalMilliseconds = (endTimeStamp.getTime() - startTimeStamp.getTime());
    var scormTime = ConvertMilliSecondsIntoSCORM2004Time(totalMilliseconds);
    ScormProcessSetValue("cmi.session_time", scormTime);
}
function recordTest(score){
    ScormProcessSetValue("cmi.score.raw", score);
    ScormProcessSetValue("cmi.score.min", "0");
    ScormProcessSetValue("cmi.score.max", "100");
    var scaledScore = score / 100;
    ScormProcessSetValue("cmi.score.scaled", scaledScore);
    if (score == 100){
        ScormProcessSetValue("cmi.success_status", "passed");
    }
    else{
        ScormProcessSetValue("cmi.success_status", "failed");
    }
}
///FUNCIONES DE CARGA
function colorearInicio(){
    ctx.beginPath()
    ctx.arc(grafo.inicio.posX, grafo.inicio.posY, radio, 0, Math.PI * 2);
    ctx.fillStyle = "green";
    ctx.fill()
}
function marcarIncompleto(){
    ScormProcessSetValue("cmi.completion_status", "incomplete");
}
function marcarCompleto(){
    ScormProcessSetValue("cmi.completion_status", "completed");
}
function ocultarRuta(){
    document.getElementById("bDibuj").style.visibility = "hidden";
}
function ocultarLabel(){
    document.getElementById("bEval").style.visibility = "hidden";
}
function llenarLabel(){
    document.getElementById("labelRecorrido").innerHTML = grafo.tipo
}
function evaluar(){
    if(grafo.tipo=="bfs") recordTest(parecidoBFS())
    else recordTest(parecidoDFS())
}
///FUNCIONES DE EVALUACION
var ruta = []
function enRuta(val){
    for(var i=0;i<ruta.length;i++){
        if(val==ruta[i]) return true
    }
    return false
}
function parecidoBFS(){
    if(ruta.length==grafo.nodos.length && ejempAct==0){
        if(grafo.inicio.id == ruta[0]){
            hijos = []
            for(var i=0;i<grafo.aristas.length;i++){
                hijos.push(grafo.aristas[i].length)
            }
            var cola = []
            cola.push(ruta[0])
            for(var i=1;i<ruta.length;i++){
                while(!vecino(cola[0],ruta[i])){
                    if(hijos[cola[0]]==0) cola.shift()
                    else return i*100/ruta.length
                }
                hijos[cola[0]]-- 
                hijos[ruta[i]]-- 
                cola.push(ruta[i])
            }
            return 100
        }
    }
    return 0;
}
function parecidoDFS(){
    if(ruta.length==grafo.nodos.length && ejempAct==0){
        if(grafo.inicio.id == ruta[0]){
            hijos = []
            for(var i=0;i<grafo.aristas.length;i++){
                hijos.push(grafo.aristas[i].length)
            }
            var pila = []
            pila.push(ruta[0])
            for(var i=1;i<ruta.length;i++){
                while(!vecino(pila[pila.length-1],ruta[i])){
                    if(hijos[pila[pila.length-1]]==0) pila.pop()
                    else return i*100/ruta.length
                }
                hijos[pila[pila.length-1]]-- 
                hijos[ruta[i]]--
                pila.push(ruta[i])
            }
            return 100
        }
    }
    return 0;
}
function vecino(izq,der){
    for(var i=0;i<grafo.aristas[izq].length;i++){
        if(grafo.aristas[izq][i]==der) return true;
    }
    return false;
}