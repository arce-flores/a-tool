//CONTROLES DE INTERFAZ
var botonPress = 0;
//4 SIMULAR DFS
//5 SIMULAR BFS
var dibujando = 0
//BOTONES
function dibujarCamino(){
    if(botonPress==0 || botonPress==3) botonPress= (botonPress+3)%6
}
function botDfs(){
    if(botonPress==0 || botonPress==4) botonPress= (botonPress+4)%8
}
function botBfs(){
    if(botonPress==0 || botonPress==5) botonPress= (botonPress+5)%10
}
function botRestaurar(){
    ejempAct = 0
    document.getElementById("bBFS").style.visibility = "hidden";
    document.getElementById("bDFS").style.visibility = "hidden";
    document.getElementById("bDibuj").style.visibility = "visible";
    ctx.clearRect(0,0,ancho,alto);
    dibujarGrafoCargado(grafoAux)
}
async function reempezar(){
    ruta = []
    await cargarGrafoCargado()
    grafoAux = grafo
    botRestaurar()
    colorearInicio()
}
//GRAFO
var grafo = {
    nodos:[],
    aristas:[],
    caminos:[],
    pintados:[],
    inicio:null,
    tipo:"dfs"
};
var nodoOrigen;
var nodoDestino;
var aristaIni
var vis
//VARIABLES DIBUJO 
var canvas = document.getElementById("generador");
var ctx= canvas.getContext("2d");
var radio = 25;
var ancho = canvas.offsetWidth
var alto = canvas.offsetHeight
var color = "white"
var ejempAct = 0
//DIBUJO POR MOUSE
function dibujarMouse(x,y){
    switch(botonPress){
        case 3:
            var nodoC = dentro(x,y)
            if(nodoC!=null){
                if(!enRuta(nodoC.id)){
                    grafo.pintados.push(nodoC)
                    ruta.push(nodoC.id)
                    ctx.beginPath();
                    ctx.arc(nodoC.posX, nodoC.posY, radio, 0, Math.PI * 2);
                    ctx.fillStyle = "blue";
                    ctx.fill()
                    if(ruta.length==grafo.nodos.length) marcarCompleto()
                }
            }
            break;
        case 4:
            var nodoD = dentro(x,y);
            if(nodoD!=null){
                visaux = new Array(grafo.nodos.length)
                vis = visaux.map(volFalse)
                lisNodM = []
                dfs(nodoD.id)
                botonPress=0
                recLis = lisNodM
                setTimeout(mosRecLis(),1500)
            }
            break;
        case 5:
            var nodoD = dentro(x,y);
            if(nodoD!=null){
                visaux = new Array(grafo.nodos.length)
                vis = visaux.map(volFalse)
                lisNodM = []
                bfs(nodoD.id)
                botonPress=0
                recLis = lisNodM
                setTimeout(mosRecLis(),1500)
            }
            break;
        case 6:
            break;
    }
}
canvas.addEventListener('click',function(event){
    var x = event.offsetX,
    y = event.offsetY;
    dibujarMouse(x,y);
})

//DIBUJO AUTOMATICO
function dibujarGrafoCargado(g){
    grafo = g
    for(var i=0;i<g.nodos.length;i++){
        var x = g.nodos[i].posX
        var y = g.nodos[i].posY
        ctx.beginPath()
        ctx.arc(x, y, radio, 0, 2 * Math.PI)
        ctx.stroke();
    }
    for(var i=0;i<g.caminos.length;i++){
        var camino = g.caminos[i]
        ctx.moveTo(camino.posIniX, camino.posIniY);
        ctx.lineTo(camino.posFinX, camino.posFinY);
        ctx.stroke();
    }
    for(var i=0;i<g.pintados.length;i++){
        var pint = g.pintados[i];
        ctx.beginPath()
        ctx.arc(pint.posX, pint.posY, radio, 0, Math.PI * 2);
        ctx.fillStyle = "blue";
        ctx.fill()
    }
}
function limpiar(){
    ctx.clearRect(0,0,ancho,alto);
    if(ejempAct==0) {
        grafoAux = grafo
        ejempAct = 1
    }
}

//Ejemplos
function ejGrafo1(){
    document.getElementById("bBFS").style.visibility = "visible";
    document.getElementById("bDFS").style.visibility = "visible";
    marcarIncompleto()
    ocultarRuta()
    ocultarLabel()
    var grafoEjemplo = {
        nodos:[{id: 0,posX:348,posY:104},
            {id: 1,posX:361,posY:235},
            {id: 2,posX:560,posY:116},
            {id: 3,posX:587,posY:321},
            {id:4,posX:154,posY:70},
            {id:5,posX:737,posY:50},
            {id:6,posX:464,posY:451},
            {id:7,posX:707,posY:432},
        ],
        aristas:[[1,2],[0],[0,3],[2]],
        caminos:[{posIniX: 348,posIniY: 104,posFinX: 361,posFinY: 235},
            {posIniX: 348,posIniY: 104,posFinX: 560,posFinY: 116},
            {posIniX: 560,posIniY: 116,posFinX: 587,posFinY: 321}
        ],
        pintados:[]
    };
    limpiar()
    dibujarGrafoCargado(grafoEjemplo)
}
function ejGrafo2(){
    document.getElementById("bBFS").style.visibility = "visible";
    document.getElementById("bDFS").style.visibility = "visible";
    marcarIncompleto()
    ocultarRuta()
    ocultarLabel()
    var grafoEjemplo = {
        nodos:[{id: 0,posX:350,posY:150},
            {id: 1,posX:400,posY:100},
            {id: 2,posX:500,posY:150},
            {id: 3,posX:350,posY:300},
            {id: 4,posX:370,posY:400},
            {id: 5,posX:470,posY:400},
            {id: 6,posX:500,posY:300}
        ],
        aristas:[[2,4,3],[1,6],[0,5],[0,6],[0,5],[4,6],[5,1]],
        caminos:[{posIniX: 350,posIniY: 150,posFinX: 500,posFinY: 150},
            {posIniX: 350,posIniY: 150,posFinX: 370,posFinY: 400},
            {posIniX: 350,posIniY: 300,posFinX: 350,posFinY: 150},
            {posIniX: 370,posIniY: 400,posFinX: 470,posFinY: 400},
            {posIniX: 470,posIniY: 400,posFinX: 500,posFinY: 150},
            {posIniX: 500,posIniY: 300,posFinX: 470,posFinY: 400},
            {posIniX: 500,posIniY: 300,posFinX: 350,posFinY: 300},
            {posIniX: 500,posIniY: 300,posFinX: 400,posFinY: 100},
            {posIniX: 400,posIniY: 100,posFinX: 400,posFinY: 100}
        ],
        pintados:[]
    };
    limpiar()
    dibujarGrafoCargado(grafoEjemplo)
}
function ejArbol(){
    document.getElementById("bBFS").style.visibility = "visible";
    document.getElementById("bDFS").style.visibility = "visible";
    marcarIncompleto()
    ocultarRuta()
    ocultarLabel()
    var grafoEjemplo = {
        nodos:[{id: 0,posX:348,posY:104},
            {id: 1,posX:361,posY:235},
            {id: 2,posX:560,posY:116},
            {id: 3,posX:380,posY:400},
            {id: 4,posX:630,posY:400},
            {id: 5,posX:300,posY:400},
            {id: 6,posX:720,posY:400}
        ],
        aristas:[[1,2],[0,3,5],[0,4,6],[1],[2],[1],[2]],
        caminos:[{posIniX: 348,posIniY: 104,posFinX: 361,posFinY: 235},
            {posIniX: 348,posIniY: 104,posFinX: 560,posFinY: 116},
            {posIniX: 361,posIniY: 235,posFinX: 380,posFinY: 400},
            {posIniX: 560,posIniY: 116,posFinX: 630,posFinY: 400},
            {posIniX: 361,posIniY: 235,posFinX: 300,posFinY: 400},
            {posIniX: 560,posIniY: 116,posFinX: 720,posFinY: 400}
        ],
        pintados:[]
    };
    limpiar()
    dibujarGrafoCargado(grafoEjemplo)
}
function ejCiclo(){
    document.getElementById("bBFS").style.visibility = "visible";
    document.getElementById("bDFS").style.visibility = "visible";
    marcarIncompleto()
    ocultarRuta()
    ocultarLabel()
    var grafoEjemplo = {
        nodos:[{id: 0,posX:348,posY:104},
            {id: 1,posX:361,posY:235},
            {id: 2,posX:560,posY:116},
            {id: 3,posX:587,posY:321}
        ],
        aristas:[[1,3],[2],[3],[0]],
        caminos:[{posIniX: 348,posIniY: 104,posFinX: 361,posFinY: 235},
            {posIniX: 361,posIniY: 235,posFinX: 560,posFinY: 116},
            {posIniX: 560,posIniY: 116,posFinX: 587,posFinY: 321},
            {posIniX: 587,posIniY: 321,posFinX: 348,posFinY: 104}
        ],
        pintados:[]
    };
    ctx.clearRect(0,0,ancho,alto);
    dibujarGrafoCargado(grafoEjemplo)
    grafo = grafoEjemplo
}
function ejNoConexo(){
    document.getElementById("bBFS").style.visibility = "visible";
    document.getElementById("bDFS").style.visibility = "visible";
    marcarIncompleto()
    ocultarRuta()
    ocultarLabel()
    var grafoEjemplo = {
        nodos:[{id: 0,posX:348,posY:104},
            {id: 1,posX:331,posY:235},
            {id: 2,posX:400,posY:235},
            {id: 3,posX:587,posY:351},
            {id: 4,posX:530,posY:400}
        ],
        aristas:[[1],[0,2],[1],[4],[3]],
        caminos:[{posIniX: 348,posIniY: 104,posFinX: 331,posFinY: 235},
            {posIniX: 331,posIniY: 235,posFinX: 400,posFinY: 235},
            {posIniX: 587,posIniY: 351,posFinX: 530,posFinY: 400}
        ],
        pintados:[]
    };
    limpiar()
    dibujarGrafoCargado(grafoEjemplo)
}
//GUARDAR GRAFO EN JSON
function guardar(){
    var dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(grafo));
    var dlAnchorElem = document.getElementById('downloadAnchorElem');
    dlAnchorElem.setAttribute("href",     dataStr     );
    dlAnchorElem.setAttribute("download", "grafo.json");
    dlAnchorElem.click();
}

//FUNCIONES DE GEOMETRIA
function dentro(x,y){
    var nodoRes=null
    for(var i=0;i<grafo.nodos.length;i++){
        var nodo = grafo.nodos[i]
        var nx = nodo.posX,
        ny = nodo.posY
        var distX = nx-x,
        distY = ny-y
        if((radio*radio)>=(distX*distX+distY*distY)) nodoRes=nodo 
    }
    return nodoRes
}
//FUNCIONES PARA LISTAS
function volFalse(value){
    value = false
}

//RECORRIDOS POR GRAFO
var glob
var myvar
function dfs(n){
    lisNodM.push(n)
    vis[n] = true
    for(var i=0;i<grafo.aristas[n].length;i++){
        var w = grafo.aristas[n][i]
        if(!vis[w]){
            dfs(w)
        }
    }
}
function bfs(n){
    lisNodM.push(n)
    vis[n] = true
    var queue = [];
    queue.push(n);
    while(queue.length>0){
        var eleg = queue.shift();
        for(var i=0;i<grafo.aristas[eleg].length;i++){
            var w = grafo.aristas[eleg][i]
            if(!vis[w]){
                vis[w] = true
                queue.push(w)
                lisNodM.push(w)
            }
        }   
    }

}
var recLis
var lisNodM
function mosRecLis() {
    if(recLis.length>0){
        nodo = grafo.nodos[recLis.shift()]
        ctx.beginPath();
        ctx.arc(nodo.posX, nodo.posY, radio, 0, Math.PI * 2);
        ctx.fillStyle = "green";
        ctx.fill()
        setTimeout(mosRecLis,1500)
    }
}
async function cargarGrafoCargado(){
    var g = await obtener("../grafo.txt");
    var g2 = JSON.parse(g);
    dibujarGrafoCargado(g2);
}
const obtener = async file => {
    const response = await fetch(file)
    const text = await response.text()
    return text.toString()
}